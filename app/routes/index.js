const messageRoutes = require('./message_routes');
const blogRoutes = require('./blog_routes');

module.exports = function(app, db) {
    messageRoutes(app, db);
    blogRoutes(app, db);
};